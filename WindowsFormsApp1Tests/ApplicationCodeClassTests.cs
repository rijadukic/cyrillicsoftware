﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApp1.Form1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Form1.Tests
{
    [TestClass()]
    public class ApplicationCodeClassTests
    {
        [TestMethod()]
        public void combineArrayStringWithSpaceTest()
        {
            string expectedResult = "Today is the wonderful day of my life";
            string[] actualStringArray = new string[] { "Today", "is", "the", "wonderful", "day", "of", "my", "life" };
            ApplicationCodeClass appObject = new ApplicationCodeClass();

            string actualResult = appObject.combineArrayStringWithSpace(actualStringArray);
            Assert.AreEqual<string>(expectedResult, actualResult);
        }
    }
}